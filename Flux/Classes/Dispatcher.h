//
//  Dispatcher.h
//  hitwe
//
//  Created by Volodymyr Kononenko on 6/3/16.
//  Copyright © 2016 Volodymyr Kononenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Action.h"

typedef NSString DispatchToken;
typedef NSString SubscribeToken;

@protocol DispatcherListener <NSObject>

@required
- (void)onDispatch:(Action *)action;

@end

@protocol DispatcherSubscriber <NSObject>

@required
- (void)dispatched;

@end

@interface DispatcherWeakSubscriber : NSObject

@property (nonatomic, weak) id<DispatcherSubscriber> subscriber;

- (instancetype)initWithSubscriber:(id<DispatcherSubscriber>)subscriber;

@end

@interface Dispatcher : NSObject

@property (nonatomic, weak) id<DispatcherSubscriber> subscriber;

- (DispatchToken *)registerListener:(id<DispatcherListener>)listener;
- (void)unregisterListener:(DispatchToken *)dispatchToken;

- (SubscribeToken *)registerSubscriber:(id<DispatcherSubscriber>)subscriber;
- (void)unregisterSubscriber:(SubscribeToken *)subscribeToken;

- (void)waitFor:(NSArray<NSString *> *)dispatchTokens;
- (void)dispatch:(Action *)action;

- (BOOL)isDispatching;

@end
