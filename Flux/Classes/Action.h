//
//  Action.h
//  hitwe
//
//  Created by Volodymyr Kononenko on 6/3/16.
//  Copyright © 2016 Volodymyr Kononenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Action : NSObject

@property (nonatomic, assign, readonly) NSInteger type;
@property (nonatomic, strong, readonly) NSDictionary *payload;

+ (instancetype)actionWithType:(NSInteger)type;
+ (instancetype)actionWithType:(NSInteger)type payload:(NSDictionary *)payload;
+ (instancetype)pendingActionWithType:(NSInteger)type payload:(NSDictionary *)payload;
+ (instancetype)successActionWithType:(NSInteger)type payload:(NSDictionary *)payload;
+ (instancetype)failureActionWithType:(NSInteger)type payload:(NSDictionary *)payload;

- (instancetype)initWithType:(NSInteger)type;
- (instancetype)initWithType:(NSInteger)type payload:(NSDictionary *)payload;

@end
