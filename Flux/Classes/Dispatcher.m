//
//  Dispatcher.m
//  hitwe
//
//  Created by Volodymyr Kononenko on 6/3/16.
//  Copyright © 2016 Volodymyr Kononenko. All rights reserved.
//

#import "Dispatcher.h"

@implementation DispatcherWeakSubscriber

- (instancetype)initWithSubscriber:(id<DispatcherSubscriber>)subscriber
{
    self = [super init];
    if (self) {
        _subscriber = subscriber;
    }
    return self;
}

@end

@interface Dispatcher ()

@property (nonatomic, strong) NSMutableDictionary<NSString *, id<DispatcherListener>> *listeners;
@property (nonatomic, assign) BOOL isDispatching;
@property (nonatomic, strong) NSMutableDictionary<NSString *, NSNumber *> *isHandled;
@property (nonatomic, strong) NSMutableDictionary<NSString *, NSNumber *> *isPending;
@property (nonatomic, assign) NSInteger lastID;
@property (nonatomic, strong) Action *pendingAction;
@property (nonatomic, strong) NSMutableDictionary *subscribers;

@end

@implementation Dispatcher

- (instancetype)init
{
    self = [super init];
    if (self) {
        _listeners = [NSMutableDictionary new];
        _isDispatching = NO;
        _isHandled = [NSMutableDictionary new];
        _isPending = [NSMutableDictionary new];
        _lastID = 0;
        _pendingAction = nil;
        _subscribers = [NSMutableDictionary new];
    }
    return self;
}

- (DispatchToken *)registerListener:(id<DispatcherListener>)listener
{
    NSString *dispatchToken = [NSString stringWithFormat:@"ID_%ld", (long)self.lastID++];
    [self.listeners setObject:listener forKey:dispatchToken];
    return dispatchToken;
}
- (void)unregisterListener:(DispatchToken *)dispatchToken
{
    [self.listeners removeObjectForKey:dispatchToken];
}

- (SubscribeToken *)registerSubscriber:(id<DispatcherSubscriber>)subscriber
{
    DispatcherWeakSubscriber *weakSubcriber = [[DispatcherWeakSubscriber alloc] initWithSubscriber:subscriber];
    SubscribeToken *token = [NSString stringWithFormat:@"%ld", (unsigned long)[subscriber hash]];
    [self.subscribers setObject:weakSubcriber forKey:token];
    return token;
}
- (void)unregisterSubscriber:(SubscribeToken *)subscribeToken
{
    [self.subscribers removeObjectForKey:subscribeToken];
}

- (void)waitFor:(nonnull NSArray<NSString *> *)dispatchTokens
{
    for (NSString *dispatchToken in dispatchTokens) {
        if ([[self.isPending objectForKey:dispatchToken] boolValue]) {
            if (![[self.isHandled objectForKey:dispatchToken] boolValue]) {
                
            }
            continue;
        }
        id<DispatcherListener> listener = [self.listeners objectForKey:dispatchToken];
        if (listener == nil) {
            
        }
        [self invokeListenerCallback:dispatchToken];
    }
}
- (void)dispatch:(Action *)action
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSAssert(_isDispatching == NO, @"Dispatch in the middle of dispatch");
        [self startDispatching:action];
        for (NSString *dispatchToken in self.listeners) {
            if ([[self.isPending objectForKey:dispatchToken] boolValue]) {
                continue;
            }
            [self invokeListenerCallback:dispatchToken];
        }
        [self stopDispatching];
        
        for (SubscribeToken *token in self.subscribers) {
            DispatcherWeakSubscriber *weakSubscriber = [self.subscribers objectForKey:token];
            [weakSubscriber.subscriber dispatched];
        }
        [self.subscriber dispatched];
    });
}

- (BOOL)isDispatching
{
    return _isDispatching;
}

#pragma mark - Private methods
- (void)invokeListenerCallback:(DispatchToken *)dispatchToken
{
    [self.isPending setObject:@(YES) forKey:dispatchToken];
    id<DispatcherListener> listener = [self.listeners objectForKey:dispatchToken];
    [listener onDispatch:self.pendingAction];
    [self.isHandled setObject:@(YES) forKey:dispatchToken];
}
- (void)startDispatching:(Action *)action
{
    for (NSString *dispatchToken in self.listeners) {
        [self.isPending setObject:@(NO) forKey:dispatchToken];
        [self.isHandled setObject:@(NO) forKey:dispatchToken];
    }
    self.pendingAction = action;
    _isDispatching = YES;
}
- (void)stopDispatching
{
    self.pendingAction = nil;
    _isDispatching = NO;
}

@end
