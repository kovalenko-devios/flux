//
//  Action.m
//  hitwe
//
//  Created by Volodymyr Kononenko on 6/3/16.
//  Copyright © 2016 Volodymyr Kononenko. All rights reserved.
//

#import "Action.h"

@implementation Action

+ (instancetype)actionWithType:(NSInteger)type
{
    return [[self alloc] initWithType:type];
}
+ (instancetype)actionWithType:(NSInteger)type payload:(NSDictionary *)payload
{
    return [[self alloc] initWithType:type payload:payload];
}
+ (instancetype)pendingActionWithType:(NSInteger)type payload:(NSDictionary *)payload
{
    return [[self alloc] initWithType:type payload:@{@"pending": payload}];
}
+ (instancetype)successActionWithType:(NSInteger)type payload:(NSDictionary *)payload
{
    return [[self alloc] initWithType:type payload:@{@"success": payload}];
}
+ (instancetype)failureActionWithType:(NSInteger)type payload:(NSDictionary *)payload
{
    return [[self alloc] initWithType:type payload:@{@"failure": payload}];
}

- (instancetype)initWithType:(NSInteger)type
{
    return [self initWithType:type payload:nil];
}
- (instancetype)initWithType:(NSInteger)type payload:(NSDictionary *)payload
{
    self = [super init];
    if (self) {
        _type = type;
        _payload = payload;
    }
    return self;
}

@end
